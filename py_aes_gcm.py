from Crypto.Cipher import AES
import sys
import os


def encrypt_AES_GCM(message, secret_key):
    aes_cipher = AES.new(secret_key, AES.MODE_GCM)
    ciphertext, auth_tag = aes_cipher.encrypt_and_digest(message)
    return ciphertext, aes_cipher.nonce, auth_tag


def decrypt_AES_GCM(encryptedMsg, secretKey):
    (ciphertext, nonce, authTag) = encryptedMsg
    aes_cipher = AES.new(secretKey, AES.MODE_GCM, nonce)
    plaintext = aes_cipher.decrypt_and_verify(ciphertext, authTag)
    return plaintext


def process(path_file, secret_key):
    file_name = os.path.basename(path_file)
    message = read_file(path_file)
    encrypted_msg = encrypt_AES_GCM(message, secret_key)
    dump_to_file(encrypted_msg, file_name)

    encrypted_msg_read = read_from_file(file_name)
    decrypted_msg = decrypt_AES_GCM(encrypted_msg_read, secret_key)
    print("decrypted message:", decrypted_msg)


def read_file(path):
    file_to_encrypt = open(path)
    message = bytes(file_to_encrypt.read(), 'UTF-8')
    file_to_encrypt.close()
    return message


def dump_to_file(encrypted_msg, file_name):
    (ciphertext, nonce, authTag) = encrypted_msg
    file_out = open(file_name + ".ciphertext", "wb")
    file_out.write(ciphertext)
    file_out.close()

    file_out = open(file_name + ".nonce", "wb")
    file_out.write(nonce)
    file_out.close()

    file_out = open(file_name + ".authTag", "wb")
    file_out.write(authTag)
    file_out.close()


def read_from_file(file_name):
    file_out = open(file_name + ".ciphertext", "rb")
    ciphertext = bytes(file_out.read())
    file_out.close()

    file_out = open(file_name + ".nonce", "rb")
    nonce = bytes(file_out.read())
    file_out.close()

    file_out = open(file_name + ".authTag", "rb")
    authTag = bytes(file_out.read())
    file_out.close()

    encrypted_msg_read = (ciphertext, nonce, authTag)
    return encrypted_msg_read


if __name__ == '__main__':
    if len(sys.argv) == 3:
        file = sys.argv[1]
        key = bytes(sys.argv[2], 'utf-8')
    else:
        print('first argument should be the file to encrypt')
        print('second argument should be the secret key')
        sys.exit()
    process(file, key)
